var mongoose = require('mongoose');
var User     = mongoose.model('User');

var bcrypt   = require('bcrypt-nodejs'); // allow us to encrypt passwords
var jwt      = require('jsonwebtoken');

module.exports.register = function(req, res) {
 
  var username = req.body.username;
  var name = req.body.name || null;
  var password = req.body.password;

  User.create({
    username: username,
    name: name,
    password: bcrypt.hashSync(password, bcrypt.genSaltSync(10))
  }, function(err, user) {
    if (err) {
      console.log(err);
      res.status(400).json(err);
    } else {
      console.log('user created', user);
      res.status(201).json(user);
    }
  });
};

module.exports.login = function(req, res) {
 
  var username = req.body.username;
  var password = req.body.password;

  User.findOne({
    username: username
  }).exec(function(err, user) {
    if (err) {
      console.log(err);
      res.status(400).json(err);
    } else {         // password is what user enters
      if (bcrypt.compareSync(password, user.password)) {
        console.log('User found', user);
        // server generates token
        var token = jwt.sign({ username: user.username }, 's3cr3t', { expiresIn: 3600 });
        
        // server sends token to user 
        res.status(200).json({success: true, token: token, user: user});
      } else {
        res.status(401).json('Unauthorized');
      }
    }
  });
};



module.exports.usersGetAll = function(req, res) {

  console.log('Read All Users');

  User.find().exec(function(err, users) {
  
      res.json(users);  // creates json response with the data 
    });

};


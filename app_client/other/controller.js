(function () {     // Building controllers for api http requests 

  // asssign controllers to myApp module 
  angular.module('myApp').controller('TestController', TestController);
  
  angular.module('myApp').controller('FacController', FacController);
  angular.module('myApp').controller('StuController', StuController);
  
  angular.module('myApp').controller('GetOneFacController', GetOneFacController);
  angular.module('myApp').controller('GetOneStuController', GetOneStuController);
  
  // Better views 
  angular.module('myApp').controller('AllFacController', AllFacController);
  
  angular.module('myApp').controller('AddFacController', AddFacController);
 
  angular.module('myApp').controller('RegisterController', RegisterController);
  angular.module('myApp').controller('LoginController', LoginController);
  angular.module('myApp').controller('AllUsersController', AllUsersController);
    
  function TestController() {
    var myModel = this;
    myModel.name = 'Test';
  
  }
  
  function FacController($http) {
    
    // for testing 
    var myModel = this;
    myModel.name = 'Faculty';
    
    $http.get('/api/faculty').then(function(response) {   // use .then instead of .success
        myModel.response = response;
       
      });
       
  }
 
  // Notice input parameter changes from $http to StudentFactory
  function StuController(StudentFactory) {
      
      // instead of running this code, 
      // would need StuController($http)   
      // var myModel = this;
      // myModel.name = 'Students:';
      // $http.get('/api/students').then(function(response) {   
      //     myModel.response = response;  
      // });
      
      // we will use StudentFactory
      var vm = this; // controller  calls the service  
      StudentFactory.getAllStudents().then(function(response) { 
        vm.students = response; 
        
        });
   }
 
   function GetOneStuController($routeParams, StudentFactory) {
      
      var vm = this;
      var id = $routeParams.id; 
      StudentFactory.getOneStudent(id).then(function(response) { 
        vm.student = response; 
        
        });
   }
 
   function GetOneFacController($http, $routeParams) {
    var vm = this;   // vm ViewModel variable 
    console.log($routeParams);
    var id =  $routeParams.id; 
    
    $http.get('/api/faculty/' + id)
      .then(function(response) {  
        vm.faculty = response.data; 
       
      });
     
    //code to delete a record 
    vm.deleteFaculty = function() {
             
        $http.delete('/api/faculty/' + $routeParams.id)       
             .then(function(response) {  
                console.log(response.status);
                window.location = "#/allfaculty";   });
               
     }   
  }
 
  function AllFacController($http) {
    
    // for testing 
    var myModel = this;
    myModel.title = 'Faculty';
    
    $http.get('/api/faculty').then(function(response) {   // use .then instead of .success
        myModel.faculty = response.data;
       
      });
       
  }
 
   function AddFacController($http) {
     var vm = this;

     vm.addFaculty = function() {
               
        //alert("inside the function");
        
        var postData = {
          name: vm.name,
          specialty: vm.s,
          institution: vm.institution
        };
    
        console.log(postData);
        $http.post('/api/faculty/', postData).then(function(response) {
                vm.newfaculty = response.data;
                console.log(response.status);
                window.location = "#/allfaculty";
      });
  };
 }
 
function RegisterController($http) {
  var vm = this;

  vm.register = function() {
    var user = {
      username: vm.username,
      password: vm.password,
      name: vm.name
    };

    $http.post('/api/register', user).then(function(result) {
          console.log(result); 
          window.location = "#/allusers";});
  }
};

function LoginController($http,$scope) {
  var vm = this;

  vm.login = function() {
    console.log("logging in");
    if (vm.username && vm.password) {
       var user = {
          username: vm.username,
          password: vm.password
        };

        // post because we need to pass data user to the api controller
        $http.post('/api/login', user).then(function(response) {
              // response.data looks like {success: true, token: token, user: user}        
          
               console.log(response.data.user);
              
                vm.isLoggedIn = true;
                vm.loginuser = response.data.user; 
              
               window.location = "#/login";
        });
  } }

    vm.logout = function() {
      vm.isLoggedIn = false;
      window.location = "#/login";
    }

} 
 
function AllUsersController($http) {
    
    var myModel = this;
    myModel.title = 'Users';
    
    $http.get('/api/users').then(function(response) {   
        myModel.users = response.data;
       
      });
       
  }
 
})();